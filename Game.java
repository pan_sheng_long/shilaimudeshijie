/**
 *  This class is the main class of the "World of Zuul" application. 
 *  "World of Zuul" is a very simple, text based adventure game.  Users 
 *  can walk around some scenery. That's all. It should really be extended 
 *  to make it more interesting!
 * 
 *  To play this game, create an instance of this class and call the "play"
 *  method.
 * 
 *  This main class creates and initialises all the others: it creates all
 *  rooms, creates the parser and starts the game.  It also evaluates and
 *  executes the commands that the parser returns.
 * 
 * @author  Michael Kölling and David J. Barnes
 * @version 2016.02.29
 */
import java.util.*;

public class Game 
{
    private Parser parser;
    private Room currentRoom;
    private Player player;   
    private Thing thing;
    private String name;
    
    //private Map<String,Thing> map;
    
    /**
     * Create the game and initialise its internal map.
     */
    public Game() 
    {
        createRooms();
        parser = new Parser();
        player = new Player();
    }

    /**
     * Create all the rooms and link their exits together.
     */
    private void createRooms()
    {
        Room damen, liangongfang, pub, lab, office,up,down;
      
        // create the rooms
        damen = new Room("你现在在大魔王的宫殿外");
        liangongfang = new Room("你到了练功房", new Thing("苹果",20));
        lab = new Room("一个粉红色的房间", new Thing("花生豆",10));
        office = new Room("酒庄", new Thing("花生豆",10));
        pub = new Room("魔王的房间");
        lab = new Room("一个暗堡");
        office = new Room("魔王的酒庄，你离魔王越近了");
        up = new Room("魔王的厨房，你离魔王越近了 ");
        down = new Room("恭喜你找到魔王，你被魔王当奸细杀死。你重生了。");
        // initialise room exits
        //Room north, Room east, Room south, Room west ,
        damen.setExit("east",liangongfang);
        damen.setExit("south",lab);
        damen.setExit("west",pub);
        liangongfang.setExit("west",damen);
        liangongfang.setExit("south",office);
        pub.setExit("east",damen);
        lab.setExit("north",damen);
        lab.setExit("esat",office);
        lab.setExit("south",up);
        office.setExit("west",lab);
        office.setExit("north",liangongfang);
        up.setExit("south",down);
        up.setExit("west",pub);
        up.setExit("east",office);
        up.setExit("north",lab);
        // outside.setExits(null, theater, lab, pub,null,null);
        // theater.setExits(null, null, null, outside,uptheater,downtheater);
        // pub.setExits(null, outside, null, null,null,null);
        // lab.setExits(outside, office, null, null,null,null);
        // 大门.setExits(null, null, null, lab,null,null);
        // uptheater.setExits(null, null, null, null,null,theater);
        // downtheater.setExits(null, null, null, null,theater,null);

        currentRoom = damen;  // start game 大门
    }

    /**
     *  Main play routine.  Loops until end of play.
     */
    public void play() 
    {            
        printWelcome();

        // Enter the main command loop.  Here we repeatedly read commands and
        // execute them until the game is over.
                
        boolean finished = false;
        while (! finished) {
            Command command = parser.getCommand();
            finished = processCommand(command);
        }
        System.out.println("感谢你的使用，宫殿欢迎你的再次到来.");
    }

    /**
     * Print out the opening message for the player.
     */
    private void printWelcome()
    {
        System.out.println();
        System.out.println("欢迎来到这个游戏的世界");
        System.out.println("在这个世界里你需要活到最后");
        System.out.println("如果你需要帮助，可以试试输入‘help’");
        System.out.println("忘了告诉你，你现在是一只史莱姆");
        System.out.println("" + currentRoom.getDescription());
        currentRoom.printExists();
    }

    /**
     * Given a command, process (that is: execute) the command.
     * @param command The command to be processed.
     * @return true If the command ends the game, false otherwise.
     */
    private boolean processCommand(Command command) 
    {
        boolean wantToQuit = false;

        if(command.isUnknown()) {
            System.out.println("我这个配置好像还不懂你是什么意思。。。");
            return false;
        }

        Word commandWord = command.getCommandWord();
        // if (commandWord.equals("help")) {
            // printHelp();
        // }
        // else if (commandWord.equals("go")) {
            // goRoom(command);
        // }
        // else if (commandWord.equals("quit")) {
            // wantToQuit = quit(command);
        // }
        // else if(commandWord.equals("pick")){
            // pick();
        // }
        // else if(commandWord.equals("look")){
            // look();
        // }
        // else if(commandWord.equals("check")){
            // check();
        // }
        // else if(commandWord.equals("eat")){
           // eat(command);
        // }
        switch(commandWord) {
           case HELP:
               printHelp();
               break;
           case GO:
               goRoom(command);
               break;
           case QUIT:
               wantToQuit = quit(command);
               break;
           case PICK:
               pick();
               break;
           case LOOK:
                look();
                break;
           case CHECK:
                check();
                break;
           case EAT:
                eat(command);
                break;
        }
        return wantToQuit;
    }
    
    public void eat(Command command){
       String name = command.getSecondWord();
       Thing thing = player.bag.get(name);
       player.eat(name);
       System.out.println("您吃了："+ thing  );
       System.out.println("您当前的能量为：" + player.getStrenght()  );
       player.bag.remove(name);
    }
    
    public void check(){
         if(player.bag.values() == null){
            System.out.println("背包里面什么也没有");
         }
         else{
            System.out.print("背包里面有:"); 
            for(Thing value : player.bag.values()){
               System.out.println(" "+value);
            }
         }  
    }
    
    public void look(){
       Thing thing = currentRoom.getTing();
            if(thing != null){
               System.out.println("你看到了一个：" + thing + "。");
            }
            else{
               System.out.println("这里什么都没有");
            }
    }
    
    public void pick(){
         Thing thing = currentRoom.getTing();
             if(thing != null){
                 player.pick(thing);
                 currentRoom.removeThing();
                 System.out.println("捡到了一个物品" + thing + "。");
             }
             else{
                System.out.println("这里没有东西可以捡");
              }
    }
    
    // implementations of user commands:

    /**
     * Print out some help information.
     * Here we print some stupid, cryptic message and a list of the 
     * command words.
     */
    private void printHelp() 
    {
        System.out.println("You are lost. You are alone. You wander");
        System.out.println("around at the university.");
        System.out.println();
        System.out.println("Your command words are:");
        System.out.println("   go quit help look pick check eat");
    }

    /** 
     * Try to go in one direction. If there is an exit, enter
     * the new room, otherwise print an error message.
     */
    private void goRoom(Command command) 
    {
        if(!command.hasSecondWord()) {
            // if there is no second word, we don't know where to go...
            System.out.println("Go where?");
            return;
        }

        String direction = command.getSecondWord();

        // Try to leave current room.
        Room nextRoom = currentRoom.goNext(direction);
        // Room nextRoom = null;
        // if(direction.equals("north")) {
            // nextRoom = currentRoom.northExit;
        // }
        // if(direction.equals("east")) {
            // nextRoom = currentRoom.eastExit;
        // }
        // if(direction.equals("south")) {
            // nextRoom = currentRoom.southExit;
        // }
        // if(direction.equals("west")) {
            // nextRoom = currentRoom.westExit;
        // }
        // if(direction.equals("up")) {
            // nextRoom = currentRoom.upExit;
        // }
        // if(direction.equals("down")) {
            // nextRoom = currentRoom.downExit;
        // }
        
        if (nextRoom == null) {
            System.out.println("这个方向你无法穿过!");
        }
        else {
            currentRoom = nextRoom;
            System.out.println("你到达了" + currentRoom.getDescription());
            currentRoom.printExists();
        }
    }

    // private void printExists(){
            // System.out.print("Exits: ");
            // if(currentRoom.northExit != null) {
                // System.out.print("north ");
            // }
            // if(currentRoom.eastExit != null) {
                // System.out.print("east ");
            // }
            // if(currentRoom.southExit != null) {
                // System.out.print("south ");
            // }
            // if(currentRoom.westExit != null) {
                // System.out.print("west ");
            // }
            // if(currentRoom.upExit != null) {
                // System.out.print("up ");
            // }
            // if(currentRoom.downExit != null) {
                // System.out.print("down ");
            // }

            // System.out.println();
    // }
    
    /** 
     * "Quit" was entered. Check the rest of the command to see
     * whether we really quit the game.
     * @return true, if this command quits the game, false otherwise.
     */
    private boolean quit(Command command) 
    {
        if(command.hasSecondWord()) {
            System.out.println("Quit what?");
            return false;
        }
        else {
            return true;  // signal that we want to quit
        }
    }
}
