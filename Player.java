import java.util.*;

public class Player
{
    private int strenght = 100;
    public Map<String,Thing> bag = new HashMap<>();
    
    public void step(){
        strenght -= 10;
    }
    
    public void pick(Thing thing){
         bag.put(thing.getName(), thing);
    }
    
    public int getStrenght(){
         return this.strenght;
    }
    
    public void eat(String name) {
          Thing thing = bag.get(name);
          if(thing != null) {
             strenght += thing.getEnergy();
             if(strenght > 100) {
                strenght = 100;
             }
          }
          
    }
    
    public boolean isDead() {
         return strenght <= 0;
    }
}





