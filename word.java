/**
 * Enumeration class Word - write a description of the enum class here
 * 
 * @author (your name here)
 * @version (version number or date here)
 */
public enum Word
{
    //MONDAY, TUESDAY, WEDNESDAY, THURSDAY, FRIDAY, SATURDAY, SUNDAY "look","pick","check","eat"
    GO("go"),QUIT("quit"),HELP("help"),PICK("pick"),LOOK("look"),
    CHECK("check"),EAT("eat");
    
    private String commandWord;
    private Word(String commandWord){
       this.commandWord = commandWord;
    }
    
    public String getCommandWord(){
           return this.commandWord;
    }
}
